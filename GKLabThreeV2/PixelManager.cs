﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GKLabThreeV2
{
    class PixelManager
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public class Color
        {
            public static int Blue => 0;
            public static int Green => 1;
            public static int Red => 2;
            public static int Alpha => 3;
        }

        private readonly WriteableBitmap _bitmap;
        private byte[] _data;

        public PixelManager(WriteableBitmap bitmap)
        {
            _bitmap = bitmap;
            Height = bitmap.PixelHeight;
            Width = bitmap.PixelWidth;
            _data = new byte[bitmap.BackBufferStride * bitmap.PixelHeight];
            bitmap.CopyPixels(_data, bitmap.BackBufferStride, 0);
        }

        public void SetColor(int x, int y, byte blue, byte green, byte red, byte alpha)
        {
            int index = y * _bitmap.BackBufferStride + x * _bitmap.Format.BitsPerPixel / 8;
            _data[index + Color.Blue] = blue;
            _data[index + Color.Green] = green;
            _data[index + Color.Red] = red;
            _data[index + Color.Alpha] = alpha;
        }

        public (byte blue, byte green, byte red, byte alpha) GetColor(int x, int y)
        {
            int index = y * _bitmap.BackBufferStride + x * _bitmap.Format.BitsPerPixel / 8;
            return (_data[index], _data[index + 1], _data[index + 2], _data[index + 3]);
        }

        public void AlterByte(int x, int y, int color, int value)
        {
            int index = y * _bitmap.BackBufferStride + x * _bitmap.Format.BitsPerPixel / 8 + color;
            byte oldValueForCheck = _data[index];
            _data[index] += (byte)value;
            if (value < 0 && _data[index] > oldValueForCheck)
            {
                _data[index] = 0;
                return;
            }
            if (value > 0 && _data[index] < oldValueForCheck)
            {
                _data[index] = 255;
                return;
            }
        }

        public void SaveToBitmap()
        {
            _bitmap.WritePixels(new System.Windows.Int32Rect(0, 0, _bitmap.PixelWidth, _bitmap.PixelHeight), _data, _bitmap.BackBufferStride, 0);
        }

        //BULK OPERATIONS
        public IEnumerable<byte> GetPixelValues(int colorOffset)
        {
            return Enumerable.Range(0, _data.Length / (_bitmap.Format.BitsPerPixel / 8)).Select(i => _data[i * _bitmap.Format.BitsPerPixel / 8 + colorOffset]);
        }

        public void SetColor(int colorOffset, List<byte> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                _data[(i * _bitmap.Format.BitsPerPixel / 8) + colorOffset] = values[i];
            }
        }
    }
}
