﻿using GKLabThreeV2.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace GKLabThreeV2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                var UriForDebugging = new Uri(openFileDialog.FileName);
                LoadImage(UriForDebugging);
            }
        }

        private void LoadDefaultImage(object sender, EventArgs e)
        {
            LoadImage(new Uri(Config.TempImagePath1, UriKind.Relative));
        }
        
        void LoadImage(Uri uri)
        {
            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.UriSource = uri;
            src.CacheOption = BitmapCacheOption.OnLoad;
            src.EndInit();

            WriteableBitmap wbitmap = new WriteableBitmap(src);
            imageOriginal.Stretch = Stretch.Fill;
            imageOriginal.Source = wbitmap;


            Task.Run(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    WriteableBitmap wb2 = null;
                    wb2 = new WriteableBitmap(src.Clone());
                    imageFloyd.Stretch = Stretch.Fill;
                    imageFloyd.Source = wb2;
                    IImageProcessor floydSteinbergProcessor = new FloydSteinbergProcessor();
                    floydSteinbergProcessor.Process(wb2, 4);
                });
            });

            Task.Run(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    WriteableBitmap wb3 = new WriteableBitmap(src.Clone());
                    imageKmeans.Stretch = Stretch.Fill;
                    imageKmeans.Source = wb3;
                    IImageProcessor kMeansProcessor = new KMeansProcessor();
                    kMeansProcessor.Process(wb3, 4);
                });

            });

            Task.Run(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    WriteableBitmap wb4 = new WriteableBitmap(src.Clone());
                    imagePopularity.Stretch = Stretch.Fill;
                    imagePopularity.Source = wb4;
                    IImageProcessor popularityProcessor = PopularityProcessor.GetBucketCountProcessor(32);
                    popularityProcessor.Process(wb4, 4);
                });
            });
        }
    }
}
