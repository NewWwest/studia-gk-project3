﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GKLabThreeV2.ImageProcessing
{
    class PopularityProcessor : IImageProcessor
    {
        public PopularityProcessorMode Mode { get; private set; }
        public int? BucketCount { get; private set; }
        public int? BucketFactor { get; private set; }

        public enum PopularityProcessorMode
        {
            BucketCount,
            BucketFactor,
            BucketFull,
        }

        public static PopularityProcessor GetBucketCountProcessor(int bucketCount)
        {
            return new PopularityProcessor()
            {
                Mode = PopularityProcessorMode.BucketCount,
                BucketCount = bucketCount
            };
        }
        public static PopularityProcessor GetBucketFactorProcessor(int bucketFactor)
        {
            return new PopularityProcessor()
            {
                Mode = PopularityProcessorMode.BucketFactor,
                BucketFactor = bucketFactor
            };
        }

        public static PopularityProcessor GetBucketFullProcessor()
        {
            return new PopularityProcessor()
            {
                Mode = PopularityProcessorMode.BucketFull,
                BucketCount = 255
            };
        }

        public PopularityProcessor()
        {
        }

        void IImageProcessor.Process(WriteableBitmap bitmap, int colorLevels)
        {
            var pm = new PixelManager(bitmap);
            DoColor(PixelManager.Color.Blue, colorLevels, pm);
            DoColor(PixelManager.Color.Green, colorLevels, pm);
            DoColor(PixelManager.Color.Red, colorLevels, pm);
            pm.SaveToBitmap();
        }

        private void DoColor(int colorOffset, int colorLevels, PixelManager pm)
        {
            List<byte> buckets = GetBuckets(colorLevels);
            buckets.Sort();
            var blueValues = pm.GetPixelValues(colorOffset).ToList();
            List<byte> pickedColors = PickColors(blueValues, buckets, colorLevels);
            MutateColors(blueValues, pickedColors);
            pm.SetColor(colorOffset, blueValues);
        }

        private void MutateColors(List<byte> values, List<byte> pickedColors)
        {
            for (int i = 0; i < values.Count; i++)
            {
                int minIndex = int.MaxValue;
                int minDistance = int.MaxValue;
                for (int j = 0; j < pickedColors.Count; j++)
                {
                    int distance = Math.Abs(values[i] - pickedColors[j]);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        minIndex = j;
                    }
                }
                values[i] = pickedColors[minIndex];
            }

        }

        private List<byte> PickColors(List<byte> values, List<byte> buckets, int colorLevels)
        {
            var dictionary = new Dictionary<byte, int>();
            foreach (byte b in buckets)
            {
                dictionary.Add(b, 0);
            }
            for (int i = 0; i < values.Count; i++)
            {
                for (int j = 0; j < buckets.Count - 1; j++)
                {
                    if (values[i] < buckets[j])
                    {
                        dictionary[buckets[j]]++;
                        break;
                    }
                }

            }

            return dictionary.OrderByDescending(kv => kv.Value).Take(colorLevels).Select(kv => kv.Key).ToList();
        }

        private List<byte> GetBuckets(int colorLevels)
        {
            var colorValues = new List<byte>();
            int bucketCount = -1;
            switch (Mode)
            {
                case PopularityProcessorMode.BucketCount:
                    bucketCount = BucketCount.Value;
                    break;
                case PopularityProcessorMode.BucketFactor:
                    bucketCount = colorLevels * BucketFactor.Value;
                    break;
                case PopularityProcessorMode.BucketFull:
                    return Enumerable.Range(0, 256).Select(i => (byte)i).ToList();
                default:
                    throw new ArgumentOutOfRangeException();
            }

            for (int i = 1; i <= bucketCount; i++)
            {
                colorValues.Add((byte)((i * 256 / bucketCount) - 1));
            }
            return colorValues;
        }
    }
}
