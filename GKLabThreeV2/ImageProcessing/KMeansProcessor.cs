﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GKLabThreeV2.ImageProcessing
{
    class KMeansProcessor : IImageProcessor
    {
        void IImageProcessor.Process(WriteableBitmap bitmap, int colorLevels)
        {
            var pm = new PixelManager(bitmap);
            DoProcess(pm, colorLevels, PixelManager.Color.Blue);
            DoProcess(pm, colorLevels, PixelManager.Color.Green);
            DoProcess(pm, colorLevels, PixelManager.Color.Red);
            pm.SaveToBitmap();
        }

        private void DoProcess(PixelManager pm, int colorLevels, int colorOffset)
        {
            var values = pm.GetPixelValues(colorOffset).Select(b => (int)b).ToList();
            var oldCenters = InitCenters(colorLevels);
            (List<int> asignments, List<int> newCenters) = AsignAndCalculate(values, oldCenters);
            int iteration = 1;
            while (PositionsDifferent(oldCenters, newCenters))
            {
                Debug.WriteLine($"Iteration {++iteration}");
                oldCenters = newCenters;
                (asignments, newCenters) = AsignAndCalculate(values, oldCenters);
            }
            var newBlues = new List<byte>(values.Count);
            for (int i = 0; i < asignments.Count; i++)
            {
                newBlues.Add((byte)newCenters[asignments[i]]);
            }
            pm.SetColor(colorOffset, newBlues);
        }

        private bool PositionsDifferent(List<int> oldCenters, List<int> newCenters)
        {
            return !newCenters.All(c => oldCenters.Contains(c));
        }

        private (List<int> asignments, List<int> centers) AsignAndCalculate(List<int> values, List<int> centers)
        {
            List<long> newCenters = Enumerable.Repeat(0L, centers.Count).ToList();
            List<int> newCentersPopularity = Enumerable.Repeat(0, centers.Count).ToList();
            List<int> asignments = Enumerable.Repeat(-1, values.Count).ToList();
            for (int i = 0; i < values.Count; i++)
            {
                int minIndex = int.MaxValue;
                int minDistance = int.MaxValue;
                for (int j = 0; j < centers.Count; j++)
                {
                    int distance = Math.Abs((values[i] - centers[j]) * (values[i] - centers[j])); //TODO: use as metric x or x squared??
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        minIndex = j;
                    }
                }
                newCenters[minIndex] += values[i];
                newCentersPopularity[minIndex]++;
                asignments[i] = minIndex;

            }
            List<int> temp = new List<int>();
            for (int i = 0; i < centers.Count; i++)
            {
                if (newCentersPopularity[i] != 0)
                    temp.Add((int)(newCenters[i] / newCentersPopularity[i]));
                else
                {
                    Debug.WriteLine("Now this is quite exceptional!");
                    temp.Add(0);
                }
            }
            return (asignments, temp);
        }

        private List<int> InitCenters(int colorLevels)
        {
            List<int> centers = new List<int>();
            var rand = new Random();
            for (int i = 0; i < colorLevels; i++)
            {
                int center = rand.Next(255);
                while (centers.Any(c => c == center))
                    center = rand.Next(255);
                centers.Add(center);
            }
            return centers;
        }
    }
}
