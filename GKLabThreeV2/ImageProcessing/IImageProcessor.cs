﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GKLabThreeV2.ImageProcessing
{
    interface IImageProcessor
    {
        void Process(WriteableBitmap bitmap, int colorLevels);
    }
}
