﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GKLabThreeV2.ImageProcessing
{
    class FloydSteinbergProcessor : IImageProcessor
    {
        void IImageProcessor.Process(WriteableBitmap bitmap, int colorLevels)
        {
            List<byte> permitterColors = GetColors(colorLevels);
            permitterColors.Sort();
            var pm = new PixelManager(bitmap);
            for (int y = 0; y < pm.Height; y++)
            {
                for (int x = 0; x < pm.Width; x++)
                {
                    (byte blue, byte green, byte red, byte alpha) = pm.GetColor(x, y);
                    (byte blueNormalized, int errorB) = Normalize(blue, permitterColors);
                    (byte greebNormalized, int errorG) = Normalize(green, permitterColors);
                    (byte redNormalized, int errorR) = Normalize(red, permitterColors);
                    //(byte alphaNormalized, int error) = Normalize(alpha, permitterColors);
                    PropegateError(pm, x, y, errorB, errorG, errorR);
                    pm.SetColor(x, y, blueNormalized, greebNormalized, redNormalized, alpha);
                }
            }
            pm.SaveToBitmap();
        }

        private void PropegateError(PixelManager pm, int x, int y, int errorB, int errorG, int errorR)
        {
            if (x + 1 < pm.Width)
            {
                pm.AlterByte(x + 1, y, PixelManager.Color.Blue, errorB * 7 / 16);
                pm.AlterByte(x + 1, y, PixelManager.Color.Green, errorG * 7 / 16);
                pm.AlterByte(x + 1, y, PixelManager.Color.Red, errorR * 7 / 16);
            }
            if (x - 1 > 0 && y + 1 < pm.Height)
            {
                pm.AlterByte(x - 1, y + 1, PixelManager.Color.Blue, errorB * 3 / 16);
                pm.AlterByte(x - 1, y + 1, PixelManager.Color.Green, errorG * 3 / 16);
                pm.AlterByte(x - 1, y + 1, PixelManager.Color.Red, errorR * 3 / 16);
            }
            if (y + 1 < pm.Height)
            {
                pm.AlterByte(x, y + 1, PixelManager.Color.Blue, errorB * 5 / 16);
                pm.AlterByte(x, y + 1, PixelManager.Color.Green, errorG * 5 / 16);
                pm.AlterByte(x, y + 1, PixelManager.Color.Red, errorR * 5 / 16);
            }
            if (x + 1 < pm.Width && y + 1 < pm.Height)
            {
                pm.AlterByte(x + 1, y, PixelManager.Color.Blue, errorB * 5 / 16);
                pm.AlterByte(x + 1, y, PixelManager.Color.Green, errorG * 5 / 16);
                pm.AlterByte(x + 1, y, PixelManager.Color.Red, errorR * 5 / 16);
            }
        }

        private List<byte> GetColors(int colorLevels)
        {
            var colorValues = new List<byte>();
            for (int i = 1; i <= colorLevels; i++)
            {
                colorValues.Add((byte)((i * 256 / colorLevels) - 1));
            }
            return colorValues;
        }

        private (byte, int) Normalize(byte value, IList<byte> orderedList)
        {
            for (int i = 0; i < orderedList.Count - 1; i++)
            {
                if (value < orderedList[i])
                {
                    return (orderedList[i], value - orderedList[i]);
                }
            }
            return (255, 0);
        }
    }
}
