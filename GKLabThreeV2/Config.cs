﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GKLabThreeV2
{
    public static class Config
    {
        public static readonly string TempImagePath1 = @"..\..\..\1380131690613.jpg";
        public static readonly string TempImagePath2 = @"..\..\..\1409164979192.jpg";
        public static readonly string TempImagePath3 = @"..\..\..\1420416082399.jpg";
        public static readonly string TempImagePath4 = @"..\..\..\1420525007669.jpg";
        public static readonly string TempImagePathRed = @"..\..\..\red.png";
        public static readonly string TempImagePathBlue = @"..\..\..\blue.png";
        public static readonly string TempImagePathPurple = @"..\..\..\purple.png";
        public static readonly string TempImagePathWhite = @"..\..\..\white.png";
        public static readonly string LoadingIconPath = @"..\..\..\loading.png";
    }
}
